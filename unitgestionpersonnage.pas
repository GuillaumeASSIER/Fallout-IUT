unit unitGestionPersonnage;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes, SysUtils, gestionEcran, unitCadre, unitReponse,unitRessources,UnitCentre;

type OccupationEnu = (Lac,Foret,Decharge,Village,Ferme,IUT,Exploration); //Possible occupation du personnage

type Personnage = record         //Type record des personnages
     PV : Integer;               //PV des personnage
     nom : String;               //Nom des personnages
     occupation : OccupationEnu; //Que fait le personnage
     Experienceferme : Integer;  //Experience obtenue à la ferme
     ExperienceDecharge:Integer; //Experience obtenue à la decharge
     ExperienceIUT:Integer;      //Experience obtenue à l'iut
     ExperienceForet:Integer;    //Experience obtenue à la foret
     ExperiencedeLac:Integer;    //Experience obtenue au lac

end;





procedure genPerso();                                                           //Génération de personnage (4*nouveauPersonnage)
procedure detailPerso();                                                        //Page des personnages
function nouveauPersonnage(Nom : String) : Personnage;                          //Fonction pour créer les personnages
procedure attributionPersonnage();                                              //Procédure qui permet à l'utilisateur de choisir quel personnage va aux differents lieux
function personneLibre():Integer;                                               //Nombre de personne libre
procedure affectationOccup(var personnage:personnage;affectation:OccupationEnu);//Procédure qui donne l'affectation du personnage
procedure RenitialisationOccup();                                               //Procédure qui renitialise les occupation des villeagois au village
procedure CaseofOccupation(var choixjoueur:personnage);                         //Procédure qui gère l'exp suivant les lieux attribué
procedure ExpFerme ;
procedure ExpDecharge;
Procedure ExpIUT   ;
procedure ExpForet ;
procedure ExpLac ;
procedure PageExp;                                                              //Page d'experience
procedure expFindetour;                                                         //Procédure qui gère l'experience en fin de tour
procedure BonusressourceEXP;                                                    //Procédure qui ajoute les ressources suivant l'experience
procedure DebloquerExp;                                                         //Procédure  qui bloque l'experience suivant les recherche effectuer

implementation

uses Unitcoordonnee,UnitMenu,unitExploration;                                   //Obligé de les mettre ici pour éviter l'erreur "circular reference"

var
   Persolibre:Integer;                                                          //Variable qui gère le nombre de personne disponible;
   Tableauperso: array [1..4] of personnage;                                    //Nos personnages sont stockés dans un tableau (plus pratique que des variables)

   //Variables qui gèrent le bonus obtenu suivant l'experience des personnages
   BonusFerrailleur:Integer;
   BonusFermier:Integer;
   BonusChercheur:Integer;
   BonusBucheron:Integer;
   BonusPecheur:Integer;

procedure genPerso();
begin
     tableauperso[1]:=nouveauPersonnage('Fabien Duc');
     tableauperso[2]:=nouveauPersonnage('Alois Mandry');
     tableauperso[3]:=nouveauPersonnage('Julie Tropcompliqué');
     tableauperso[4]:=nouveauPersonnage('Guillaume Assier');
end;

procedure detailPerso();

var repPerso:string;
begin
 //décor
 effacerecran();
 cadre(double);
 //texte avec coordonnée
 deplacerCurseurXY(1,1);
 writeln('Personnage : ',tableauperso[1].nom);
 deplacerCurseurXY(50,1);
 writeln('Occupation actuelle : ',tableauperso[1].occupation);

 deplacerCurseurXY(1,2);
 writeln('Personnage : ',tableauperso[2].nom);
 deplacerCurseurXY(50,2);
 writeln('Occupation actuelle : ',tableauperso[2].occupation);

 deplacerCurseurXY(1,3);
 writeln('Personnage : ',tableauperso[3].nom);
 deplacerCurseurXY(50,3);
 writeln('Occupation actuelle : ',tableauperso[3].occupation);

 deplacerCurseurXY(1,4);
 writeln('Personnage : ',tableauperso[4].nom);
 deplacerCurseurXY(50,4);
 writeln('Occupation actuelle : ',tableauperso[4].occupation);


 deplacerCurseurXY(1,9);
writeln('1: Fiche Experience');
 deplacerCurseurXY(1,10);
 writeln('2: Retour');

 //boucle pour la réponse
 while (repPerso<>('1')) AND (repPerso<>('2')) do
      begin
            posRep(repPerso);
            case repPerso of
             '1' : PageExp()   ;      //Si rep=1 alors on retourne au menu
             '2' : menuPrincipal()   ;//Si rep=1 alors on retourne au menu

            else ecrireEnPosition(getErreurCoord,'Erreur');
            end;
      end;


end;

function nouveauPersonnage(Nom : String) : Personnage;
begin
  nouveauPersonnage.nom := Nom;           //attribut le nom au type personnage
  nouveauPersonnage.PV := 100;            //attribut 100PV au type personnage
  nouveauPersonnage.occupation:=Village;  //Attribut par défaut des villageois
  //Exeperience à 0 au debut de partie
  nouveauPersonnage.Experienceferme:=0;
  nouveauPersonnage.ExperienceDecharge:=0;
  nouveauPersonnage.ExperienceIUT:=0 ;
  nouveauPersonnage.ExperienceForet:=0;
  nouveauPersonnage.ExperiencedeLac:=0;
end;

procedure attributionPersonnage();
var repPerso:string;   //variable réponse menu
begin
//initialisation
 repPerso:='0';
//décor
effacerecran();
cadre(double);
//texte avec coordonnée
deplacerCurseurXY(1,1);
writeln('Qui voulez-vous envoyer : ')  ;
deplacerCurseurXY(1,2);
writeln('1 : ',tableauperso[1].nom);
deplacerCurseurXY(50,2);
writeln('Occupation actuelle : ',tableauperso[1].occupation);

deplacerCurseurXY(1,3);
writeln('2 : ',tableauperso[2].nom);
deplacerCurseurXY(50,3);
writeln('Occupation actuelle : ',tableauperso[2].occupation);

deplacerCurseurXY(1,4);
writeln('3 : ',tableauperso[3].nom);
deplacerCurseurXY(50,4);
writeln('Occupation actuelle : ',tableauperso[3].occupation);

deplacerCurseurXY(1,5);
writeln('4 : ',tableauperso[4].nom);
deplacerCurseurXY(50,5);
writeln('Occupation actuelle : ',tableauperso[4].occupation);


deplacerCurseurXY(1,10);
writeln('6: Retour');



while (repPerso<>('1')) and (repPerso<>('2'))and(repPerso<>('3')) and(repPerso<>('4')) and(repPerso<>('6'))  do
      begin
           posRep(repPerso);
           case repPerso of
                '1' : CaseofOccupation(tableauperso[1]);            //Si rep=1 alors attribu le lieu choisi au joueur1
                '2' : CaseofOccupation(tableauperso[2]);            //Si rep=2 alors attribu le lieu choisi au joueur2
                '3' : CaseofOccupation(tableauperso[3]);            //si rep=3 alors attribu le lieu choisi au joueur3
                '4' : CaseofOccupation(tableauperso[4]);            //si rep=4 alors attribu le lieu choisi au joueur4
                '6' : menuprincipal();                              //si rep=6  alors retour menu

           else ecrireEnPosition(getErreurCoord,'Erreur');          // pour tout autre nombre entré alors afficher "erreur"
           end;





      end;
end;

function personneLibre():Integer;
var
   i:Integer; //Variable pour pas boucle pour
begin
     Persolibre:=0 ;
     for i:=1 to length(tableauperso) do
         begin
              if tableauperso[i].occupation=Village then Persolibre:=PersoLibre+1;
         end;
         personneLibre:=Persolibre ;
end;

procedure RenitialisationOccup();
begin
     tableauperso[1].occupation:=Village;
     tableauperso[2].occupation:=Village;
     tableauperso[3].occupation:=Village;
     tableauperso[4].occupation:=Village;

     BonusFerrailleur:=0;
     BonusFermier:=0;
     BonusChercheur:=0;
     BonusBucheron:=0;
     BonusPecheur:=0;
end;

procedure CaseofOccupation(var choixjoueur:personnage);
begin

     if(choixjoueur.occupation=Village)then
     affectationOccup(choixjoueur,getLieuchoisipourattribution)
     else
    begin
      ecrireEnPosition(getErreurCoord,'Deja assigné, choisir quelqu''un d''autre');
      readln();
      attributionPersonnage();

    end;

end;

procedure affectationOccup(var personnage:personnage;affectation:OccupationEnu);
begin
     personnage.occupation:=affectation;
end;

procedure ExpFerme ;
var
   i:Integer; //Varible pour pas boucle pour
 begin

      for i:=1 to length(tableauperso) do
      begin
           if tableauperso[i].occupation=Ferme then
           begin
                tableauperso[i].Experienceferme:=tableauperso[i].Experienceferme+1;
                BonusFermier:=BonusFermier + (tableauperso[i].Experienceferme div 3);
           end;
      end;
 end;

procedure ExpDecharge ;
var
   i:Integer;//Varible pour pas boucle pour
 begin

      for i:=1 to length(tableauperso) do
       begin
          if tableauperso[i].occupation=Decharge then
           begin
            tableauperso[i].Experiencedecharge:=tableauperso[i].Experiencedecharge+1;
            BonusFerrailleur:=BonusFerrailleur + (tableauperso[i].Experiencedecharge div 3);
           end;
       end;
 end;

procedure ExpIUT ;
var
   i:Integer;//Varible pour pas boucle pour
 begin

      for i:=1 to length(tableauperso) do
       begin
            if tableauperso[i].occupation=IUT then
            begin
                 tableauperso[i].ExperienceIUT:=tableauperso[i].ExperienceIUT+1;
                 BonusChercheur:=BonusChercheur + (tableauperso[i].ExperienceIUT div 3);
            end;
       end;
 end;

procedure ExpForet;
var
   i:Integer;//Varible pour pas boucle pour
 begin

 for i:=1 to length(tableauperso) do
         begin
              if tableauperso[i].occupation=Foret then
              begin
              tableauperso[i].ExperienceForet:=tableauperso[i].ExperienceForet+1;
              BonusBucheron:=BonusBucheron + (tableauperso[i].ExperienceForet div 3);
              end;
         end;
 end;

procedure ExpLac;
var
   i:Integer;         //Varible pour pas boucle pour
 begin

      for i:=1 to length(tableauperso) do
         begin
              if tableauperso[i].occupation=Lac then
              begin
                   tableauperso[i].ExperiencedeLac:=tableauperso[i].ExperiencedeLac+1;
                   BonusPecheur:=BonusPecheur + (tableauperso[i].Experiencedelac div 3);
              end;

         end;
end;

procedure expFindetour;
 begin
  ExpForet();
  ExpIUT();
  ExpDecharge();
  ExpFerme();
  ExpLac();
 end;

procedure PageExp();
 var repExp:String;
 begin
      repexp:='0';
      //décor
      effacerecran();
      cadre(double);
      //texte avec coordonnée
      deplacerCurseurXY(1,1);
      deplacerCurseurXY(1,2);
      writeln(tableauperso[1].nom);
      deplacerCurseurXY(1,3);
      writeln('Fermier : ',tableauperso[1].Experienceferme);
      deplacerCurseurXY(1,4);
      writeln('Recherche : ',tableauperso[1].ExperienceIUT);
      deplacerCurseurXY(1,5);
      writeln('Bucheron : ',tableauperso[1].Experienceforet);
      deplacerCurseurXY(1,6);
      writeln('Ferailleur : ',tableauperso[1].Experiencedecharge);
      deplacerCurseurXY(1,7);
      writeln('Pecheur : ',tableauperso[1].ExperiencedeLac);

      deplacerCurseurXY(25,2);
      writeln(tableauperso[2].nom);
      deplacerCurseurXY(25,3);
      writeln('Fermier : ',tableauperso[2].Experienceferme);
      deplacerCurseurXY(25,4);
      writeln('Recherche : ',tableauperso[2].ExperienceIUT);
      deplacerCurseurXY(25,5);
      writeln('Bucheron : ',tableauperso[2].Experienceforet);
      deplacerCurseurXY(25,6);
      writeln('Ferailleur : ',tableauperso[2].Experiencedecharge);
      deplacerCurseurXY(25,7);
      writeln('Pecheur : ',tableauperso[2].ExperiencedeLac);

      deplacerCurseurXY(49,2);
      writeln(tableauperso[3].nom);
      deplacerCurseurXY(49,3);
      writeln('Fermier : ',tableauperso[3].Experienceferme);
      deplacerCurseurXY(49,4);
      writeln('Recherche : ',tableauperso[3].ExperienceIUT);
      deplacerCurseurXY(49,5);
      writeln('Bucheron : ',tableauperso[3].Experienceforet);
      deplacerCurseurXY(49,6);
      writeln('Ferailleur : ',tableauperso[3].Experiencedecharge);
      deplacerCurseurXY(49,7);
      writeln('Pecheur : ',tableauperso[3].ExperiencedeLac);

      deplacerCurseurXY(73,2);
      writeln(tableauperso[4].nom);
      deplacerCurseurXY(73,3);
      writeln('Fermier : ',tableauperso[4].Experienceferme);
      deplacerCurseurXY(73,4);
      writeln('Recherche : ',tableauperso[4].ExperienceIUT);
      deplacerCurseurXY(73,5);
      writeln('Bucheron : ',tableauperso[4].Experienceforet);
      deplacerCurseurXY(73,6);
      writeln('Ferailleur : ',tableauperso[4].Experiencedecharge);
      deplacerCurseurXY(73,7);
      writeln('Pecheur : ',tableauperso[4].ExperiencedeLac);

      deplacerCurseurXY(1,9);
      writeln('Vous obtenez 1 point d''expérience par personnage par lieu visités, le niveau d''expérience est actuellement bloqué au 0');
      deplacerCurseurXY(1,10);
      writeln('niveau 10');
      deplacerCurseurXY(1,11);
      writeln('L''experience acquise vous permettra de produire une ressource supplémentaire tous les 3 points obtenus');
      deplacerCurseurXY(1,13);
      writeln('Constuisez votre centre technique pour débloquer le niveau 20 et 30');
      deplacerCurseurXY(1,15);
      writeln('1: Retour');



      while (repExp<>('1')) do
       begin
            posrep(repExp);
            case repExp of
                 '1' : detailPerso;

            else ecrireEnPosition(getErreurCoord,'Erreur');                          // pour tout autre nombre entré alors afficher "erreur"
            end;
       end;
 end;

procedure BonusressourceEXP;
begin

     setFood(getfood+bonuspecheur);
     deplacerCurseurXY(1,8);
     writeln('Bonus d''expérience de pécheur : ',bonuspecheur,' nourritures');
     setFood(getfood+BonusFermier);
     deplacerCurseurXY(1,9);
     writeln('Bonus d''expérience de fermier : ',BonusFermier,' nourritures');
     setSteel(getsteel+BonusFerrailleur);
     deplacerCurseurXY(1,10);
     writeln('Bonus d''expérience de ferailleur : ',BonusFerrailleur,' aciers');
     setWood(getwood+BonusBucheron);
     deplacerCurseurXY(1,11);
     writeln('Bonus d''expérience de bucheron : ',BonusBucheron,' bois');
     deplacerCurseurXY(1,12);
     setComposant(getcomposant+BonusChercheur);
     writeln('Bonus d''expérience de recherche : ',BonusChercheur,' composants');
end;

procedure DebloquerExp;
var
   i:Integer; // Variable travaille boucle pour
begin
     //ballayage de toute les personnage
     for i:=1 to length(tableauperso) do
     begin
          //condition d'achat d'amelioration
          if (getEtatlevel3=false) and (getEtatlevel2=false) then

                begin
                //bloquage des valeur suivant amelioration
                if tableauperso[i].Experienceferme >10 then tableauperso[i].Experienceferme:=10 ;
                if tableauperso[i].ExperienceDecharge >10 then tableauperso[i].ExperienceDecharge:=10;
                if tableauperso[i].ExperienceIUT  >10  then tableauperso[i].ExperienceIUT:=10;
                if tableauperso[i].ExperienceForet >10  then tableauperso[i].ExperienceForet:=10;
                if tableauperso[i].ExperiencedeLac >10 then tableauperso[i].ExperiencedeLac:=10;
                end
          //condition d'achat d'amelioration
     else if (getEtatlevel3=false) and (getEtatlevel2=true) then
                begin
                //bloquage des valeur suivant amelioration
                if tableauperso[i].Experienceferme >20 then tableauperso[i].ExperiencedeLac:=20 ;
                if tableauperso[i].ExperienceDecharge> 20 then tableauperso[i].ExperienceDecharge:=20;
                if tableauperso[i].ExperienceIUT  >20  then tableauperso[i].ExperienceIUT:=20;
                if tableauperso[i].ExperienceForet >20  then tableauperso[i].ExperienceForet:=20;
                if tableauperso[i].ExperiencedeLac >20 then tableauperso[i].ExperiencedeLac:=20;
                end
     //condition d'achat d'amelioration
     else  if (getEtatlevel3=True) and (getEtatlevel2=True) then
             begin
                  //bloquage des valeur suivant amelioration
                if tableauperso[i].Experienceferme >30 then tableauperso[i].Experienceferme:=30 ;
                if tableauperso[i].ExperienceDecharge> 30 then tableauperso[i].ExperienceDecharge:=30;
                if tableauperso[i].ExperienceIUT  >30  then tableauperso[i].ExperienceIUT:=30;
                if tableauperso[i].ExperienceForet >30  then tableauperso[i].ExperienceForet:=30;
                if tableauperso[i].ExperiencedeLac >30 then tableauperso[i].ExperiencedeLac:=30;
             end;
     end;
end;

end.


