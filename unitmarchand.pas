unit unitMarchand;

{$mode objfpc}{$H+}  {$codepage UTF8}

interface

uses
  Classes,SysUtils,GestionEcran,unitCadre,unitCoordonnee,unitRessources,UnitReponse;

procedure achatBois();                                                          //ProcedureS d'achat et vente au marchand
procedure achatNourriture();
procedure achatAcier();
procedure achatComposant();
procedure venteBois();
procedure venteNourriture();
procedure venteAcier();
procedure VenteComposant();
procedure marchand();                                                           //Gère l'apparition du marchand
procedure aleaMarchand(pourcentageMarchand:integer);                            //Gère les pourcentages d'apparition du marchand
implementation

uses
  unitMenu;

procedure achatBois();
begin
  if getMoney < 2 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez d''argent, appuyer sur entrée pour continuer');
     readln();
     end
     else
       begin
       setMoney(getMoney -2);
       setWood(getWood + 10);
       end;
  marchand();
end;

procedure achatNourriture();
begin
  if getMoney < 3 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez d''argent, appuyer sur entrée pour continuer');
     readln();
     end
     else
       begin
       setFood(getFood + 10);
       setMoney(getMoney -3);
       end;
  marchand();
end;

procedure achatAcier();
begin
  if getMoney < 4 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez d''argent, appuyer sur entrée pour continuer');
     readln();
     end
     else
       begin
       setSteel(getSteel + 10);
       setMoney(getMoney -4);
       end;
  marchand();
end;

procedure achatComposant();
begin
   if getMoney < 8 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez d''argent, appuyer sur entrée pour continuer');
     readln();
     end
     else
       begin
       setcomposant(getcomposant + 1);
       setMoney(getMoney -8);
       end;
  marchand();
end;

procedure venteBois();
begin
  if getWood < 10 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez de bois, appuyer sur entrée pour continuer');
     readln();
     end
     else
       begin
       setWood(getWood - 10);
       setMoney(getMoney +1);
       end;
  marchand();
end;

procedure venteNourriture();
begin
  if getFood < 10 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez de Nourriture, appuyer sur entrée pour continuer');
     readln();
     end
  else
      begin
      setFood(getFood - 10);
      setMoney(getMoney +2);
      end;
  marchand();
end;

procedure venteAcier();
begin
  if getSteel< 10 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez d''acier, appuyer sur entrée pour continuer');
     readln();
     end
  else
      begin
      setSteel(getSteel - 10);
      setMoney(getMoney +3);
      marchand();
      end;
  marchand();
end;

procedure venteComposant();
begin
  if getComposant< 1 then
     begin
     ecrireEnPosition(getErreurCoord,'Vous n''avez pas assez de composant, appuyer sur entrée pour continuer');
     readln();
     end
  else
      begin
      setComposant(getComposant - 10);
      setMoney(getMoney +6);
      marchand();
      end;
  marchand();
end;

procedure marchand();
var
  repMarchand : string;
begin
  repMarchand:='0';
  effacerEcran;
  deplacerCurseurXY(1,1);
  writeln('Achat et vente de ressource par lot de 10');

  cadre(double);
  deplacerCurseurXY(1,2);
  writeln('1: Acheter du bois (10)(2 pièces)');
  deplacerCurseurXY(1,3);
  writeln('2: Acheter de la nourriture (10)(3 pièces)');
  deplacerCurseurXY(1,4);
  writeln('3: Acheter de l''acier (10)(4 pièces)');
  deplacerCurseurXY(1,5);
  writeln('4: Acheter un composant (1)(8 pièces)');

  deplacerCurseurXY(1,6);
  writeln('5: Vendre du bois (10)(1 pièces)');
  deplacerCurseurXY(1,7);
  writeln('6: Vendre de la nourriture (10)(2 pièces)');
  deplacerCurseurXY(1,8);
  writeln('7: Vendre de l''acier (10)(3 pièces)');
  deplacerCurseurXY(1,9);
  writeln('8: Vendre un composant (1)(6 pièces)');
  deplacerCurseurXY(1,11);
  writeln('9: Ne rien faire');

  synthese();

  // boucle de la réponse
  while (repMarchand<>('1')) AND (repMarchand<>('2')) AND (repMarchand<>('3')) AND (repMarchand<>('4')) AND (repMarchand<>('5')) AND (repMarchand<>('6')) AND (repMarchand<>('7'))AND (repMarchand<>('8'))AND (repMarchand<>('9')) do
        begin
             posrep(repMarchand);
             case repMarchand of
                  '1' : achatBois;                                                     //Achat bois
                  '2' : achatNourriture;                                               //Achat nourriture
                  '3' : achatAcier;                                                    //Achat acier
                  '4' : achatcomposant;
                  '5' : venteBois;                                                     //Vente bois
                  '6' : venteNourriture;                                               //Vente nourriture
                  '7' : venteAcier();                                                  //Vente acier
                  '8' : ventecomposant;
                  '9' : menuPrincipal;                                                      //Quitter le marchand
             else ecrireEnPosition(getErreurCoord,'Erreur');                           //Pour tout autre nombre entré alors afficher "erreur"
             end;
        end
end;

procedure aleaMarchand(pourcentageMarchand:integer);
begin
     randomize;
     if random(100)+1<=pourcentageMarchand then marchand();
end;

end.

