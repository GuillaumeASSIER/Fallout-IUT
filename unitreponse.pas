unit unitReponse;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes, SysUtils,gestionEcran,unitCoordonnee;

procedure posRep(var reponse:string);                                           //procédure qui place le curseur pour la réponse

implementation

procedure posRep(var reponse:string);                                           //procédure qui place le curseur pour la réponse
begin
     //Cadre
     dessinerCadre(getcadrerepcoord1,getcadrerepcoord2,double,10,0) ;
     //Question
     ecrireEnPosition(getinicoord,'Entrer un numéro : ') ;
     //Repositionne l'écran
     deplacerCurseurXY(0,0);
     //Coordonnée de la réponse
     deplacerCurseurXY(20,28);
     //Affichage
     readln(reponse);
end;

end.

